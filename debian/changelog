jssc (2.8.0-4) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with Java 21 (Closes: #1053036)
  * Removed the -java-doc package
  * Standards-Version updated to 4.6.2

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 29 Sep 2023 09:32:16 +0200

jssc (2.8.0-3) unstable; urgency=medium

  * Team upload.
  * Set Java source version to 1.7 (Closes: #981964)
  * Use debhelper-compat 13
  * Bump Standards-Version to 4.5.1
  * Set Rules-Requires-Root: no in debian/control

 -- tony mancill <tmancill@debian.org>  Sun, 07 Feb 2021 13:42:08 -0800

jssc (2.8.0-2) unstable; urgency=medium

  * Team upload.
  * Fixed a build failure caused by a regression in jh_build argument parsing
  * Standards-Version updated to 4.4.0
  * Switch to debhelper level 11
  * Use salsa.debian.org Vcs-* URLs
  * Properly clean the .so file generated
  * Marked the native libraries to be excluded from the upstream tarball

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 29 Jul 2019 13:14:05 +0200

jssc (2.8.0-1) unstable; urgency=medium

  * Team upload.

  [ Scott Howard ]
  * Updated watch file, added get-orig-source target to rules

  [ tony mancill ]
  * New upstream version.  (Closes: #825744)
  * Bump Standards-Version to 3.9.8.
  * Update upstream URL.
  * Update Vcs URLs to use HTTPS.

 -- tony mancill <tmancill@debian.org>  Mon, 06 Jun 2016 21:39:43 -0700

jssc (2.6.0-5) unstable; urgency=medium

  * Fix FTBFS on hurd and kfreebsd* by setting java source 1.6 
    and specifying UTF-8 encoding in debian/rules

 -- Scott Howard <showard@debian.org>  Sat, 24 May 2014 20:59:33 -0400

jssc (2.6.0-4) unstable; urgency=medium

  * Fix FTBFS on kfreebsd* from not finding jni_nd.h

 -- Scott Howard <showard@debian.org>  Sat, 24 May 2014 18:58:59 -0400

jssc (2.6.0-3) unstable; urgency=medium

  * Use g++, not cc (i.e., gcc). (Closes: #748920)
  * Lintian override "source-is-missing." Source is included, just named
    differently.

 -- Scott Howard <showard@debian.org>  Sat, 24 May 2014 16:27:08 -0400

jssc (2.6.0-2) unstable; urgency=medium

  * Team upload
  * Make it possible to retrieve information about the USB device that
    provides the serial port (Closes: #734820)

 -- David Prévot <taffit@debian.org>  Sat, 11 Jan 2014 15:27:42 -0400

jssc (2.6.0-1) unstable; urgency=low

  * Initial release. (Closes: #731189)

 -- Scott Howard <showard@debian.org>  Mon, 16 Dec 2013 11:23:17 -0500
